DateTimeRangeFilterBundleBundle
=======================

DateTimeRangeType Datagrid Filter for Sonata Admin MongoDB

Installation
------------

``` bash
$ composer require goncziakos/datetimerange-filter-bundle
```

Symfony 3.4:
``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    $bundles = [
        // ...
        new GoncziAkos\DateTimeRangeFilterBundle\GoncziAkosDateTimeRangeFilterBundle(),
    ];
}
```
Symfony 4.x:

``` php
<?php
// config/bundles.php

return [
    GoncziAkos\DateTimeRangeFilterBundle\GoncziAkosDateTimeRangeFilterBundle::class => ['all' => true],
];
```

Usage
-------------

Format optional filter option.

``` php
<?php

// ...
use Sonata\AdminBundle\Admin\AbstractAdmin;
use GoncziAkos\DateTimeRangeFilterBundle\Form\Type\Filter\DateTimeRangeType;
// ...

class XYAdmin extends AbstractType
{
    $datagridMapper
        ->add('createdAt', DateTimeRangeType::class)
    ;

```

Format optional filter option.

``` php
    $datagridMapper
        ->add('createdAt', DateTimeRangeType::class, [
            'format' => 'yyyy-MM-dd HH:mm'
        ])
    ;
```

License
-------

This bundle is released under the LGPL license. 
