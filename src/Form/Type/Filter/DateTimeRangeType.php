<?php


namespace GoncziAkos\DateTimeRangeFilterBundle\Form\Type\Filter;

use Doctrine\ODM\MongoDB\Query\Builder;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\Type\Filter\DateTimeRangeType as BaseDateTimeRangeType;
use Sonata\DoctrineMongoDBAdminBundle\Filter\Filter;
use Sonata\Form\Type\DateTimeRangePickerType;
use DateTimeInterface;

class DateTimeRangeType extends Filter
{
    /**
     * {@inheritdoc}
     */
    public function filter(ProxyQueryInterface $queryBuilder, $alias, $field, $values): void
    {
        if (!$this->isActive()) {
            return;
        }

        $type = isset($values['type']) ? $values['type'] : null;

        switch ($type) {
            case BaseDateTimeRangeType::TYPE_NOT_BETWEEN:
                $this->applyTypeIsNotBetween($queryBuilder, $field, $values);
                return;
            default: // DateTimeRangeType::TYPE_BETWEEN
                $this->applyTypeIsBetween($queryBuilder, $field, $values);
        }
    }

    public function isActive(): bool
    {
        $values = $this->getValue();

        return isset($values['value']['start'])
            && isset($values['value']['end'])
            && $values['value']['start'] instanceof DateTimeInterface
            && $values['value']['end'] instanceof DateTimeInterface;
    }

    private function applyTypeIsBetween(ProxyQueryInterface $queryBuilder, string $field, array $values): void
    {
        /** @var Builder $queryBuilder */
        $queryBuilder
            ->field($field)
            ->range($values['value']['start'], $values['value']['end']);
    }

    private function applyTypeIsNotBetween(ProxyQueryInterface $queryBuilder, string $field, array $values): void
    {
        /** @var Builder $queryBuilder */
        $queryBuilder
            ->addOr($queryBuilder->expr()->field($field)->lt($values['value']['start']))
            ->addOr($queryBuilder->expr()->field($field)->gt($values['value']['end']));
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultOptions()
    {
        return [
            'field_type' => DateTimeRangePickerType::class,
            'format' => 'yyyy-MM-dd HH:mm:ss'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getRenderSettings()
    {
        $format = $this->getOption('format');

        if ($format) {
            $options = (array)$this->getFieldOption('field_options');
            $options['format'] = $format;
            $this->setFieldOption('field_options', $options);
        }

        return [
            BaseDateTimeRangeType::class,
            [
                'field_type' => $this->getFieldType(),
                'field_options' => $this->getFieldOptions(),
                'label' => $this->getLabel(),
            ]
        ];
    }

}